The following queries are implemented using flora:

1. Find all pairs of contestants who happened to audition the same piece during the same show
and got the same score from at least one judge.
This is a warm-up rule for what follows.

2. Find all pairs of contestants who happened to audition the same piece (in possibly different
shows) and got the same average score for that piece.
This query involves aggregates.

3. Find all pairs of contestants who auditioned the same piece in (possibly different) shows that
had at least 3 judges and the two contestants got the same highest score.
This query also involves aggregates.

4. Find all pairs of contestants such that the first contestants has performed all the pieces of the
second contestant (possibly in different shows)
Write this query in two different ways:
(a) without the use of quantifiers; and
(b) using explicit quantifiers (in this case, forall).
Query 4(b) involves complex use of negation (\naf) and 4(a) uses explicit quantifiers (which is
significantly simpler). Flora-2 is one of the very few systems that supports explicit quantifiers.

5. Find all chained co-auditions. A chained co-auditions is the transitive closure of the following
binary relation: X and Y (directly) co-auditioned iff they both performed the same piece in the
same show and got the same score from at least one (same) judge. Thus, a chained co-audition
can be either a direct or an indirect co-audition.
This query involves recursion.